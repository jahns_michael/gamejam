class_name Arrow
extends Area2D

export(int) var speed = 30

onready var kill_timer = $KillTimer

var direction := Vector2.ZERO

func _ready():
	kill_timer.start()

func _physics_process(_delta: float) -> void:
	if direction != Vector2.ZERO:
		var velocity = direction * speed
		global_position += velocity

func set_direction(in_direction: Vector2) -> void:
	direction = in_direction
	rotation += direction.angle()


func _on_KillTimer_timeout():
	queue_free()
