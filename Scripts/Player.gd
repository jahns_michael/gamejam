class_name Player
extends KinematicBody2D

signal player_shot_arrow(arrow)

export (int) var speed = 500
export (float) var acceleration = 0.1
export (float) var friction = 0.5

var velocity:Vector2 = Vector2()
var arrow_speed:int = 2000
var arrow:PackedScene = preload("res://Scenes/Arrow.tscn")

onready var end_of_bow:Position2D = $EndOfBow
onready var bow_direction:Position2D = $BowDirection

onready var sprite:AnimatedSprite = $Sprite

func _ready():
	sprite.animation = Global.player_color


func get_input() -> Vector2:
	var input_axis:Vector2 = Vector2()
	
	if Input.is_action_pressed("up"):
		input_axis.y -= 1
	if Input.is_action_pressed("down"):
		input_axis.y += 1
	if Input.is_action_pressed("left"):
		input_axis.x -= 1
	if Input.is_action_pressed("right"):
		input_axis.x += 1

	return input_axis.normalized() * speed
	

func _physics_process(_delta) -> void:
	var input_axis = get_input()
	if input_axis.length() > 0:
		velocity = velocity.linear_interpolate(input_axis, acceleration)
	else:
		velocity = velocity.linear_interpolate(Vector2.ZERO, friction)
	velocity = move_and_slide(velocity)
	look_at(get_global_mouse_position())
		
func _unhandled_input(event) -> void:
	if event.is_action_released("lmb"):
		shoot()

func shoot() -> void:
	var arrow_instance = arrow.instance()
	var direction = (bow_direction.global_position - end_of_bow.global_position).normalized()
	emit_signal("player_shot_arrow", arrow_instance, end_of_bow.global_position, direction)


func _on_HitArea_body_entered(body):
	if "Enemy" in body.name:
		return get_tree().change_scene(str("res://Scenes/ChooseColour.tscn"))
	if "ApproachIndicator" in body.name:
		body.queue_free()

func _on_ApproachIndicatorArea_body_entered(body):
	if "Enemy" in body.name:
		print("enter on indicator")
		body.approaching = true
		
func _on_OffIndicatorArea_body_entered(body):
	if "Enemy" in body.name:
		print("enter off indicator")
		body.approaching = false
