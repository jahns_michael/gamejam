class_name Enemy
extends KinematicBody2D

onready var sprite = $AnimatedSprite
onready var timer = $Timer
var indicator:PackedScene = preload("res://Scenes/ApproachIndicator.tscn")

var motion:Vector2 = Vector2()
var active:bool = false
var player:Player
var speed:float = 300
var colour:String
var colours = ["red", "blue", "yellow"]
var approaching = false
var time = false

func _ready():
	colour = colours[randi() % colours.size()]
	sprite.animation = colour

func on_Timer_timeout():
	time = false
	spawn_indicator()

func _physics_process(delta):
	if active :
		var dir = (player.global_position - global_position).normalized()
		motion = dir.normalized() * speed * delta
		global_position += motion
		look_at(player.global_position)
	if approaching and not time :
		timer.start()
		time = true

func spawn_indicator():
	print("spawned")
	var new_indicator = indicator.instance()
	new_indicator.position = global_position
	add_child(new_indicator)		

func _on_HitArea_area_entered(area):
	if "Arrow" in area.name:
		get_node("../../point").play()
		if Global.player_color == "cream":
			return
		if colour == "red":
			if Global.player_color == "red":
				Global.red_point += 2
			else : 
				Global.red_point += 1
		elif colour == "blue":
			if Global.player_color == "blue":
				Global.blue_point += 2
			else : 
				Global.blue_point += 1
		elif colour == "yellow":
			if Global.player_color == "yellow":
				Global.yellow_point += 2
			else : 
				Global.yellow_point += 1
		queue_free()


func _on_ActiveArea_body_entered(body):
	if "Player" in body.name:
		active = true
		player = body


func _on_ActiveArea_body_exited(body):
	if "Player" in body.name:
		active = false
