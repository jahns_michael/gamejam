extends Node

var red_point:int = 5
var blue_point:int = 5
var yellow_point:int = 5
var spawner:int = 5

var player_color = "white"

func reset() -> void:
	red_point = 5
	blue_point = 5
	yellow_point = 5
	spawner = 5
	player_color = "white"
