class_name Spawner
extends StaticBody2D

onready var spawn_timer = $SpawnTimer
onready var spawn_positions = $SpawnPositions

var enemy:PackedScene = preload("res://Scenes/Enemy.tscn")

func _ready():
	spawn_timer.start()


func _on_SpawnTimer_timeout():
	spawn()

func spawn() -> void:
	var new_enemy = enemy.instance()
	var spawn_position = spawn_positions.get_child(randi() % spawn_positions.get_child_count()).position
	new_enemy.position = spawn_position
	add_child(new_enemy)


func _on_Area2D_area_entered(area):
	if "Arrow" in area.name:
		if Global.player_color == "cream":
			Global.spawner -= 1
			queue_free()
