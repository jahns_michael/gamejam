extends Area2D

var motion:Vector2 = Vector2()
var player:Player
var speed:float = 1000

func _ready():
	player = get_node("../../../Player")
	print("ready")

func _physics_process(delta):
	var dir = (player.global_position - global_position).normalized()
	motion = dir.normalized() * speed * delta
	global_position += motion
	look_at(player.global_position)
