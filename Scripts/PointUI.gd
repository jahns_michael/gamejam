extends CanvasLayer

onready var red = $RedLabel
onready var blue = $BlueLabel
onready var yellow = $YellowLabel
onready var spawner = $SpawnerLabel

func _process(_delta):
	red.text = str(Global.red_point)
	blue.text = str(Global.blue_point)
	yellow.text = str(Global.yellow_point)
	spawner.text = "Spawner: " + str(Global.spawner)
