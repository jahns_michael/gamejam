extends Node2D

func change_color_and_replay(color:String):
	Global.player_color = color
	get_tree().change_scene(str("res://Scenes/GameWorld.tscn"))

func _on_WhiteButton_pressed():
	if Global.red_point >= 5 and Global.blue_point >= 5 and Global.yellow_point >= 5:
		Global.red_point -= 5
		Global.blue_point -= 5
		Global.yellow_point -= 5
		change_color_and_replay("white")


func _on_RedButton_pressed():
	if Global.red_point >= 10 and Global.yellow_point >= 5:
		Global.red_point -= 10
		Global.yellow_point -= 5
		change_color_and_replay("red")


func _on_BlueButton_pressed():
	if Global.blue_point >= 10 and Global.yellow_point >= 5:
		Global.blue_point -= 10
		Global.yellow_point -= 5
		change_color_and_replay("blue")


func _on_YelowButton_pressed():
	if Global.yellow_point >= 10 and Global.red_point >= 5:
		Global.yellow_point -= 10
		Global.red_point -= 5
		change_color_and_replay("yellow")


func _on_CreamButton_pressed():
	if Global.red_point >= 10 and Global.blue_point >= 10 and Global.yellow_point >= 10:
		Global.red_point -= 10
		Global.blue_point -= 10
		Global.yellow_point -= 10
		change_color_and_replay("cream")


func _on_EndGame_pressed():
	get_tree().change_scene(str("res://Scenes/Menu.tscn"))
