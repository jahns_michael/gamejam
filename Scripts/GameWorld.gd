class_name GameWorld
extends Node2D

onready var player = $Player
onready var arrow_manager = $ArrowManager
onready var bgm = $bgm

func _ready() -> void:
	player.connect("player_shot_arrow", arrow_manager, "handle_arrow_spawned")
	bgm.play()
	
	
func _process(_delta):
	if Global.spawner == 0:
		get_tree().change_scene(str("res://Scenes/Menu.tscn"))
