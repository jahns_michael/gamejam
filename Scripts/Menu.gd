extends Node2D

onready var bgm = $bgm

func _ready():
	bgm.play()

func _on_StartButton_pressed():
	Global.reset()
	get_tree().change_scene(str("res://Scenes/ChooseColour.tscn"))

func _on_TutsButton_pressed():
	get_tree().change_scene(str("res://Scenes/Tuts.tscn"))

func _on_ExitButton_pressed():
	get_tree().quit()
